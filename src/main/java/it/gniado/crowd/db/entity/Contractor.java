package it.gniado.crowd.db.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Contractor {

	private String name;
	private String street;
	private String number;
	private String zip;
	private String city;
	private String taxNumber;

	public Contractor() {
	}

	public Contractor(String name, String street, String number, String zip, String city, String taxNumber) {
		this.name = name;
		this.street = street;
		this.number = number;
		this.zip = zip;
		this.city = city;
		this.taxNumber = taxNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (!(o instanceof Contractor))
			return false;

		Contractor that = (Contractor) o;

		return new EqualsBuilder()
				.append(name, that.name)
				.append(street, that.street)
				.append(number, that.number)
				.append(zip, that.zip)
				.append(city, that.city)
				.append(taxNumber, that.taxNumber)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(name)
				.append(street)
				.append(number)
				.append(zip)
				.append(city)
				.append(taxNumber)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append("name", name)
				.append("street", street)
				.append("number", number)
				.append("zip", zip)
				.append("city", city)
				.append("taxNumber", taxNumber)
				.toString();
	}

}

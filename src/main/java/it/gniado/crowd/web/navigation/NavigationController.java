package it.gniado.crowd.web.navigation;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

@ManagedBean
@SessionScoped
@Named
public class NavigationController {

	public String loginNavigation(boolean isLogged){
		return isLogged ? "/views/secured/home.xhtml" : "/login.xhtml";
	}

	public String login(){
		return "/views/login.xhtml";
	}

}

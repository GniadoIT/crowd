package it.gniado.crowd.web.controller;

import java.util.Objects;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import it.gniado.crowd.web.model.LoginModel;
import it.gniado.crowd.web.navigation.NavigationController;

@ManagedBean
@Named
public class LoginController {

	@Inject
	private LoginModel loginModel;
	@Inject
	private NavigationController navigationController;

	public String login(){
		if (Objects.equals(loginModel.getLogin(), "login") && Objects.equals(loginModel.getPassword(), "pass")) {
			loginModel.setLogged(true);
		}
		return navigationController.loginNavigation(loginModel.isLogged());
	}

	public String logout(){
		loginModel.setLogged(false);
		return navigationController.login();
	}

}
